﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($" -----------------MAIN MENU---------------- ");
            Console.WriteLine($"********************************************");
            Console.WriteLine($"*                                          *");
            Console.WriteLine($"*           1.Date Calculator              *");
            Console.WriteLine($"*           2.Calculate Grade Average      *");
            Console.WriteLine($"*           3.Generate A Random Number     *");
            Console.WriteLine($"*           4.Rate Favourite Food          *");
            Console.WriteLine($"*                                          *");
            Console.WriteLine($"********************************************");
            Console.WriteLine($"   Please select a number and press enter   ");

            var ans = Console.ReadLine();
            int choice = 0;
            if (int.TryParse(ans, out choice))
            {
                switch (choice)
                {
                    case 1:
                        // Baljinder's code here
                        Datecalc();
                        Years();
                        break;

                    case 2:
                        //Sanket's code here
                        GradeAverage();

                        break;

                    case 3:
                        //Terrence's code here

                        break;

                    case 4:
                        //Karan's code here


                        break;

                    default:

                        break;
                }
            }
            else
            {
                Console.WriteLine("You must type numeric value only!!! \n" +
                "Press any key for exit");
                Console.ReadKey();
            }
        }
        static void Datecalc()
        { // Baljinder's method starts here 
            var date = DateTime.Today;

            Console.WriteLine($"Please enter your date of birth! (dd/mm/yyyy)");
            DateTime dob = DateTime.Parse(Console.ReadLine());
            Console.WriteLine($"Your date of birth is {dob}");

            Console.WriteLine($" Today's date is {date}");
            var days = date - dob;
            Console.WriteLine($"You are {days} days old now.");
        }
        static void Years()
        {
            int number;
            Console.WriteLine($" Type any number of year");
            number = int.Parse(Console.ReadLine());
            // Baljinder's method ends here
        }
        static void GradeAverage()
        { // sanket's method here
            string level;
            string papers;
            int studentID;
            int paperCode;
            int firstNumber;
            int secondNumber;
            int thirdNumber;
            int Sum;



            Console.Write("Please enter What level(level5 or level6) Your are?: ");
            level = Console.ReadLine();


            Console.Write("Please enter three papers marks With comma and full stop  Your are enrolled in level{0} ?: ", level);
            papers = Console.ReadLine();

            Console.Write("Please enter First Marks: ");
            firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter Second Marks: ");
            secondNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter Third Marks: ");
            thirdNumber = Convert.ToInt32(Console.ReadLine());


            Console.Write("Please enter Your Student ID!:  ");
            studentID = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter Your PaperCode:  ");
            paperCode = Convert.ToInt32(Console.ReadLine());

            Sum = (firstNumber + secondNumber + thirdNumber) / 3;
            Console.ReadLine();

            Console.WriteLine("Hello, here  are the details of your level, papers, and Mark . ON Your Leve {0}, IN  {1}, with Student ID {2},  Your Papercode  is {3}, Your Final marks that you got {4},%marks", level, papers, studentID, paperCode, Sum);

            Console.ReadLine();

            if (Sum >= 50)
            {
                Console.WriteLine($"Great you have passed!");

            }

            else
            {
                Console.WriteLine($"Sorry you are failed!");
            }

            Console.ReadLine();
        }
    }
}
